package au.com.chakma.rpn.action;

import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.NullOperand;
import au.com.chakma.rpn.model.Operand;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * Created by nabarunchakma on 14/5/17.
 */
public class DivisionOperatorTest {
    DivisionOperator divisionOperator;

    @Before
    public void setUp() throws Exception {
        divisionOperator = new DivisionOperator();
    }

    @Test
    public void execute() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("13.00")));
        rpnStack.push(new NullOperand(new BigDecimal("-13.00")));
        Assert.assertTrue(rpnStack.size() == 2);
        divisionOperator.execute(rpnStack);
        Assert.assertTrue(rpnStack.size() == 1);
        Assert.assertEquals(new BigDecimal("-1"), rpnStack.peek().getResult());

        rpnStack.clear();
        rpnStack.push(new NullOperand(BigDecimal.valueOf(6.0)));
        rpnStack.push(new NullOperand(BigDecimal.valueOf(34.0)));
        divisionOperator.execute(rpnStack);
        
    }

    @Test
    public void validate() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("12.00")));
        rpnStack.push(new NullOperand(new BigDecimal("-13.00")));
        divisionOperator.validate(rpnStack);
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsNull() throws Exception {
        divisionOperator.validate(null);
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsEmpty() throws Exception {
        divisionOperator.validate(new Stack<Operand>());
    }

}