package au.com.chakma.rpn.action;

import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.NullOperand;
import au.com.chakma.rpn.model.Operand;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * Created by nabarunchakma on 14/5/17.
 */
public class OperatorTest {
    Operator operator;

    @Before
    public void setUp() throws Exception {
        operator = new Operator(null, 0) {
            @Override
            public void execute(Stack<Operand> rpnStack) throws NotEnoughOperandException, MissingStackException {

            }

            @Override
            void validate(Stack<Operand> rpnStack) throws NotEnoughOperandException {

            }
        };
    }

    @Test
    public void getName() throws Exception {
        operator.setName("Operator");
        Assert.assertNotNull(operator.getName());
    }

    @Test
    public void setName() throws Exception {
        operator.setName("Operator");
        Assert.assertTrue("Operator".equalsIgnoreCase(operator.getName()));
    }

    @Test
    public void getRequiredOperand() throws Exception {
        operator.setRequiredOperand(2);
        Assert.assertTrue(2 == operator.getRequiredOperand());
    }

    @Test
    public void setRequiredOperand() throws Exception {
        operator.setRequiredOperand(2);
        Assert.assertTrue(2 == operator.getRequiredOperand());
    }

    @Test
    public void pop() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        Operand operand = new NullOperand(new BigDecimal("12.0"));
        rpnStack.push(operand);
        Assert.assertEquals(operand, operator.pop(rpnStack));
    }
}