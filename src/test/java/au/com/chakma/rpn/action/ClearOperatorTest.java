package au.com.chakma.rpn.action;

import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.NullOperand;
import au.com.chakma.rpn.model.Operand;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * Created by nabarunchakma on 14/5/17.
 */
public class ClearOperatorTest {
    ClearOperator clearOperator;

    @Before
    public void setUp() throws Exception {
        clearOperator = new ClearOperator();
    }

    @Test
    public void execute() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("12.00")));
        rpnStack.push(new NullOperand(new BigDecimal("13.00")));
        Assert.assertTrue(rpnStack.size() == 2);
        clearOperator.execute(rpnStack);
        Assert.assertTrue(rpnStack.size() == 0);
    }

    @Test
    public void validate() throws Exception {
        clearOperator.validate(new Stack<Operand>());
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsNull() throws Exception {
        clearOperator.validate(null);
    }
}