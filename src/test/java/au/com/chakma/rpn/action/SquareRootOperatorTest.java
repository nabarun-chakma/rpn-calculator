package au.com.chakma.rpn.action;

import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.BigDecimalFactory;
import au.com.chakma.rpn.model.NullOperand;
import au.com.chakma.rpn.model.Operand;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * Created by nabarunchakma on 14/5/17.
 */
public class SquareRootOperatorTest {
    SquareRootOperator squareRootOperator;

    @Before
    public void setUp() throws Exception {
        squareRootOperator = new SquareRootOperator();
    }

    @Test
    public void execute() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("12.00")));
        rpnStack.push(new NullOperand(new BigDecimal("16.00")));
        Assert.assertTrue(rpnStack.size() == 2);
        squareRootOperator.execute(rpnStack);
        Assert.assertTrue(rpnStack.size() == 2);
        Assert.assertTrue(BigDecimalFactory.create(4).equals(rpnStack.peek().getResult()));
    }

    @Test
    public void validate() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("12.00")));
        rpnStack.push(new NullOperand(new BigDecimal("-13.00")));
        squareRootOperator.validate(rpnStack);
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsNull() throws Exception {
        squareRootOperator.validate(null);
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsEmpty() throws Exception {
        squareRootOperator.validate(new Stack<Operand>());
    }

}