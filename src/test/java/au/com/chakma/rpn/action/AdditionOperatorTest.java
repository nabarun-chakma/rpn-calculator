package au.com.chakma.rpn.action;

import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.NullOperand;
import au.com.chakma.rpn.model.Operand;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * Created by nabarunchakma on 14/5/17.
 */
public class AdditionOperatorTest {
    AdditionOperator additionOperator;

    @Before
    public void setUp() throws Exception {
        additionOperator = new AdditionOperator();
    }

    @Test
    public void execute() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("12.00")));
        rpnStack.push(new NullOperand(new BigDecimal("13.00")));
        Assert.assertTrue(rpnStack.size() == 2);
        additionOperator.execute(rpnStack);
        Assert.assertTrue(rpnStack.size() == 1);
        Assert.assertEquals(new BigDecimal("25.00"), rpnStack.peek().getResult());
    }

    @Test
    public void validate() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("12.00")));
        rpnStack.push(new NullOperand(new BigDecimal("-13.00")));
        additionOperator.validate(rpnStack);
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsNull() throws Exception {
        additionOperator.validate(null);
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsEmpty() throws Exception {
        additionOperator.validate(new Stack<Operand>());
    }

}