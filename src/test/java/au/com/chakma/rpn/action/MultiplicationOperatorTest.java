package au.com.chakma.rpn.action;

import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.NullOperand;
import au.com.chakma.rpn.model.Operand;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * Created by nabarunchakma on 14/5/17.
 */
public class MultiplicationOperatorTest {
    MultiplicationOperator multiplicationOperator;

    @Before
    public void setUp() throws Exception {
        multiplicationOperator = new MultiplicationOperator();
    }

    @Test
    public void execute() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("13.00")));
        rpnStack.push(new NullOperand(new BigDecimal("-12.00")));
        Assert.assertTrue(rpnStack.size() == 2);
        multiplicationOperator.execute(rpnStack);
        Assert.assertTrue(rpnStack.size() == 1);
        Assert.assertEquals(new BigDecimal("-156.0000"), rpnStack.peek().getResult());
    }

    @Test
    public void validate() throws Exception {
        Stack<Operand> rpnStack = new Stack<>();
        rpnStack.push(new NullOperand(new BigDecimal("12.00")));
        rpnStack.push(new NullOperand(new BigDecimal("-13.00")));
        multiplicationOperator.validate(rpnStack);
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsNull() throws Exception {
        multiplicationOperator.validate(null);
    }

    @Test(expected = NotEnoughOperandException.class)
    public void validateGivenRPNStackIsEmpty() throws Exception {
        multiplicationOperator.validate(new Stack<Operand>());
    }
}