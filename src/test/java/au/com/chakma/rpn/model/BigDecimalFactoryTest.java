package au.com.chakma.rpn.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by nabarunchakma on 16/5/17.
 */
public class BigDecimalFactoryTest {
    @Test
    public void create() throws Exception {
        BigDecimal aBigDecimal = BigDecimalFactory.create("12");
        Assert.assertTrue(BigDecimalFactory.create(12).equals(aBigDecimal));
        Assert.assertEquals(15, aBigDecimal.scale());
    }

    @Test
    public void create1() throws Exception {
        BigDecimal aBigDecimal = BigDecimalFactory.create(12);
        Assert.assertTrue(BigDecimalFactory.create("12").equals(aBigDecimal));
        Assert.assertEquals(15, aBigDecimal.scale());
    }

}