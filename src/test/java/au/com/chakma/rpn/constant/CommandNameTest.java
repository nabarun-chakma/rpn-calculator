package au.com.chakma.rpn.constant;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by nabarunchakma on 14/5/17.
 */
public class CommandNameTest {
    @Test
    public void isCommand() throws Exception {
        Assert.assertTrue(CommandName.isCommand(CommandName.ADDITION_COMMAND));
        Assert.assertTrue(CommandName.isCommand(CommandName.SUBTRACTION_COMMAND));
        Assert.assertTrue(CommandName.isCommand(CommandName.MULTIPLICATION_COMMAND));
        Assert.assertTrue(CommandName.isCommand(CommandName.DIVISION_COMMAND));
        Assert.assertTrue(CommandName.isCommand(CommandName.CLEAR_COMMAND));
        Assert.assertTrue(CommandName.isCommand(CommandName.UNDO_COMMAND));
        Assert.assertTrue(CommandName.isCommand(CommandName.SQUARE_ROOT_COMMAND));
    }

}