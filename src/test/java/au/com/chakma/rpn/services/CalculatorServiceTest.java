package au.com.chakma.rpn.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nabarunchakma on 16/5/17.
 */
public class CalculatorServiceTest {
    CalculatorService calculatorService;

    @Before
    public void setUp() throws Exception {
        calculatorService = new CalculatorService();
    }

    @Test
    public void execute() throws Exception {
        List<String> inputs = new ArrayList<>();
        inputs.add("1");
        inputs.add("2");
        inputs.add("3");
        inputs.add("4");
        inputs.add("5");
        inputs.add("6");
        inputs.add("7");
        inputs.add("8");
        inputs.add("9");
        inputs.add("10");
        inputs.add("/");
        inputs.add("+");
        inputs.add("-");
        inputs.add("*");
        inputs.add("sqrt");
        inputs.add("sqrt");

        Assert.assertEquals("[1 2 3 4 5 1.8865381215 ]", calculatorService.execute(inputs));

        inputs.clear();
        inputs.add("undo");
        inputs.add("undo");
        inputs.add("undo");
        Assert.assertEquals("[1 2 3 4 5 6 2.1111111111 ]", calculatorService.execute(inputs));

        inputs.clear();
        inputs.add("undo");
        inputs.add("undo");
        inputs.add("undo");
        Assert.assertEquals("[1 2 3 4 5 6 7 8 9 10 ]", calculatorService.execute(inputs));

    }

    @Test(expected = Exception.class)
    public void executeGivenItThrowsNotEnoughOperandException() throws Exception {
        List<String> inputs = new ArrayList<>();
        inputs.add("+");
        calculatorService.execute(inputs);
    }

    @Test(expected = Exception.class)
    public void executeGivenItThrowsInvalidOperatorNameException() throws Exception {
        List<String> inputs = new ArrayList<>();
        inputs.add("unknown");
        calculatorService.execute(inputs);
    }

    @Test
    public void stackContent() throws Exception {
        List<String> inputs = new ArrayList<>();
        inputs.add("1");
        inputs.add("2");
        inputs.add("3");
        inputs.add("+");
        String response = calculatorService.execute(inputs);
        Assert.assertEquals("[1 5 ]", calculatorService.stackContent());
    }

}