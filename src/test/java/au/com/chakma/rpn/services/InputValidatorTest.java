package au.com.chakma.rpn.services;

import au.com.chakma.rpn.exception.InvalidInputException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by nabarunchakma on 14/5/17.
 */
public class InputValidatorTest {
    InputValidator inputValidator;

    @Before
    public void setUp() throws Exception {
        inputValidator = new InputValidator();
    }

    @Test
    public void validate() throws Exception {
        List<String> inputs = Arrays.asList("10", "12.0", "+12.00", "-12.0", "+", "-", "*", "/", "undo", "sqrt", "clear");
        inputValidator.validate(inputs);
    }

    @Test(expected = InvalidInputException.class)
    public void validateGivenInvalidNumberFormat() throws Exception {
        List<String> inputs = Arrays.asList("23.0.0");
        inputValidator.validate(inputs);
    }

    @Test(expected = InvalidInputException.class)
    public void validateGivenInvalidCommand() throws Exception {
        List<String> inputs = Arrays.asList("invalid");
        inputValidator.validate(inputs);
    }

}