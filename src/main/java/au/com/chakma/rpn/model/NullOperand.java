package au.com.chakma.rpn.model;

import java.math.BigDecimal;

/**
 * Created by nabarunchakma on 16/5/17.
 */
public class NullOperand implements Operand {
    private BigDecimal result;

    public NullOperand(BigDecimal operand) {
        this.result = operand;
    }

    @Override
    public BigDecimal getResult() {
        return result;
    }
}
