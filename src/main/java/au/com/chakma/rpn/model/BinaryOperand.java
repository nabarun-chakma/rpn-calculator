package au.com.chakma.rpn.model;

import au.com.chakma.rpn.action.Operator;

import java.math.BigDecimal;

/**
 * Created by nabarunchakma on 16/5/17.
 */
public class BinaryOperand implements Operand {
    private BigDecimal result;
    private Operator operator;
    private Operand operandOne;
    private Operand operandTwo;

    public BinaryOperand(Operator operator, Operand operandOne, Operand operandTwo, BigDecimal result) {
        this.operator = operator;
        this.operandOne = operandOne;
        this.operandTwo = operandTwo;
        this.result = result;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Operand getOperandOne() {
        return operandOne;
    }

    public void setOperandOne(Operand operandOne) {
        this.operandOne = operandOne;
    }

    public Operand getOperandTwo() {
        return operandTwo;
    }

    public void setOperandTwo(Operand operandTwo) {
        this.operandTwo = operandTwo;
    }

    @Override
    public BigDecimal getResult() {
        return this.result;
    }
}
