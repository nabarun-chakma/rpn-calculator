package au.com.chakma.rpn.model;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * It helps to instantiate a BigDecimal with Scale 15 and Half Even rounding.
 *
 * Created by nabarunchakma on 16/5/17.
 */
public final class BigDecimalFactory {
    private static final int SCALE = 15;

    private BigDecimalFactory() {
        
    }

    public static BigDecimal create(String unscaledValue) {
        BigDecimal aBigDecimal = new BigDecimal(unscaledValue, MathContext.DECIMAL64);
        return aBigDecimal.setScale(SCALE, RoundingMode.HALF_EVEN);
    }

    public static BigDecimal create(double unscaledValue) {
        BigDecimal aBigDecimal = new BigDecimal(unscaledValue, MathContext.DECIMAL64);
        return aBigDecimal.setScale(SCALE, RoundingMode.HALF_EVEN);
    }
}

