package au.com.chakma.rpn.model;

import java.math.BigDecimal;

/**
 * Created by nabarunchakma on 16/5/17.
 */
public interface Operand {
    BigDecimal getResult();
}
