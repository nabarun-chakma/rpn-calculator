package au.com.chakma.rpn.constant;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by nabarunchakma on 11/5/17.
 */
public final class CommandName {
    public static final String ADDITION_COMMAND = "+";
    public static final String SUBTRACTION_COMMAND = "-";
    public static final String MULTIPLICATION_COMMAND = "*";
    public static final String DIVISION_COMMAND = "/";
    public static final String UNDO_COMMAND = "undo";
    public static final String SQUARE_ROOT_COMMAND = "sqrt";
    public static final String CLEAR_COMMAND = "clear";

    private static final List<String> COMMANDS = new ArrayList<>();

    static {
        COMMANDS.add(ADDITION_COMMAND);
        COMMANDS.add(SUBTRACTION_COMMAND);
        COMMANDS.add(MULTIPLICATION_COMMAND);
        COMMANDS.add(DIVISION_COMMAND);
        COMMANDS.add(UNDO_COMMAND);
        COMMANDS.add(SQUARE_ROOT_COMMAND);
        COMMANDS.add(CLEAR_COMMAND);
    }

    private CommandName() {
        
    }

    /**
     * returns whether the input is a valid command.
     * 
     * @param str the string to be checked
     * @return true if str is a valid command, otherwise false.
     */
    public static boolean isCommand(String str) {
        return COMMANDS.contains(str);
    }
}
