package au.com.chakma.rpn.services;

import au.com.chakma.rpn.action.Operator;
import au.com.chakma.rpn.action.OperatorFactory;
import au.com.chakma.rpn.exception.InvalidOperatorNameException;
import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.BigDecimalFactory;
import au.com.chakma.rpn.model.NullOperand;
import au.com.chakma.rpn.model.Operand;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * A Service class to act as an orchestrator to process the input.
 *
 * Created by nabarunchakma on 14/5/17.
 */
public class CalculatorService {
    private static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#############.##########");
    private Stack<Operand> rpnStack = new Stack<>();
    private OperatorFactory operatorFactory = new OperatorFactory();

    /**
     * execute the operation and return the content of the stack.
     * @param inputs the input to be processed
     * @return the stack content
     * @throws Exception error processing the inputs
     */
    public String execute(final List<String> inputs) throws Exception {
        int position = 0;
        for (String input : inputs) {
            position++;
            if (isDouble(input)) {
                rpnStack.add(new NullOperand(BigDecimalFactory.create(input)));
            } else {
                try {
                    Operator operator = operatorFactory.findByName(input);
                    operator.execute(rpnStack);
                } catch (NotEnoughOperandException e) {
                    throw new Exception("operator " + input + " (position: " + position + "): insufficient parameters");
                } catch (InvalidOperatorNameException e) {
                    throw new Exception("operator " + input + " (position: " + position + "): invalid operator");
                } catch (MissingStackException e) {
                    throw new Exception("The Stack is null");
                }
            }
        }
        return stackContent();
    }

    /**
     * returns the Stack content.
     * @return the Stack content
     */
    public String stackContent() {
        if (this.rpnStack == null) {
            return "";
        }
        Stack<Operand> clonedRpnStack = (Stack<Operand>) this.rpnStack.clone();

        List<String> output = new ArrayList<>();

        while (!clonedRpnStack.isEmpty()) {
            output.add(String.format("%s ", format(clonedRpnStack.pop().getResult())));
        }

        Collections.reverse(output);

        return output.toString().replaceAll(", ", "");
    }

    private boolean isDouble(final String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private String format(BigDecimal value) {
        return NUMBER_FORMAT.format(value);
    }
}
