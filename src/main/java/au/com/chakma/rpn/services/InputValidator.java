package au.com.chakma.rpn.services;

import au.com.chakma.rpn.constant.CommandName;
import au.com.chakma.rpn.exception.InvalidInputException;

import java.util.List;

/**
 * validates a list of string as either a double operand or a valid operator
 * 
 * Created by nabarunchakma on 14/5/17.
 */
public class InputValidator {

    /**
     * validates the inputs entries to be either <code>Double</code> or a valid command.
     * It does not change the input and the order in the array.
     * @throws InvalidInputException if a entry is invalid
     * @param inputs the values to be checked
     */
    public void validate(final List<String> inputs) throws InvalidInputException {
        String errorMessage = "%s at position %s is invalid";

        for (int i = 0; inputs != null && i < inputs.size(); i++) {
            try {
                Double.valueOf(inputs.get(i));
            } catch (NumberFormatException nfe) {
                if (!CommandName.isCommand(inputs.get(i))) {
                    throw new InvalidInputException(String.format(errorMessage, inputs.get(i), i + 1));
                }
            }
        }
        
    }
}
