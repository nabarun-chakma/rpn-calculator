package au.com.chakma.rpn;

import au.com.chakma.rpn.services.CalculatorService;

import java.io.Console;
import java.util.Arrays;

/**
 * Created by nabarunchakma on 15/5/17.
 */
public class RPNCalculator {

    public static void main(String[] args) {
        RPNCalculator calculator = new RPNCalculator();
        calculator.run();
    }

    void run() {
        Console console = System.console();
        CalculatorService calculatorService = new CalculatorService();
        String line = console.readLine("Q to quit => ");
        while (!"q".equalsIgnoreCase(line)) {
            try {
                String stackContent = calculatorService.execute(Arrays.asList(line.split(" ")));
                console.printf("Stack: %s\n", stackContent);
            } catch (Exception e) {
                console.printf("%s\n", e.getMessage());
                console.printf("Stack: %s\n", calculatorService.stackContent());
            }
            line = console.readLine("Q to quit => ");
        }
    }
}
