package au.com.chakma.rpn.action;

import au.com.chakma.rpn.exception.InvalidOperatorNameException;

import java.util.ArrayList;
import java.util.List;

/**
 * holds the definitions of operation.
 *
 * Created by nabarunchakma on 10/5/17.
 */
public class OperatorFactory {
    private List<Operator> operators = new ArrayList<>();

    public OperatorFactory() {
        operators.add(new AdditionOperator());
        operators.add(new SubtractionOperator());
        operators.add(new MultiplicationOperator());
        operators.add(new DivisionOperator());
        operators.add(new SquareRootOperator());
        operators.add(new ClearOperator());
        operators.add(new UndoOperator());
    }

    /**
     * returns an operator for the given name.
     *
     * @param operatorName the case insensative name of the operator
     * @return an operator
     * @throws InvalidOperatorNameException if no operator found
     */
    public Operator findByName(String operatorName) throws InvalidOperatorNameException {
        for (Operator operator : operators) {
            if (operator.getName().equalsIgnoreCase(operatorName)) {
                return operator;
            }
        }

        throw new InvalidOperatorNameException();
    }
}
