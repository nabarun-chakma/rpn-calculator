package au.com.chakma.rpn.action;

import au.com.chakma.rpn.constant.CommandName;
import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.BigDecimalFactory;
import au.com.chakma.rpn.model.Operand;
import au.com.chakma.rpn.model.UnaryOperand;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * find square root of the top oprerand and pushes to the stack
 * Created by nabarunchakma on 12/5/17.
 */
public class SquareRootOperator extends Operator {
    public SquareRootOperator() {
        super(CommandName.SQUARE_ROOT_COMMAND, 1);
    }

    @Override
    public void execute(final Stack<Operand> rpnStack) throws NotEnoughOperandException, MissingStackException {
        validate(rpnStack);
        Operand operandOne = pop(rpnStack);
        BigDecimal result = BigDecimalFactory.create(Math.sqrt(operandOne.getResult().doubleValue()));
        UnaryOperand operand = new UnaryOperand(new SquareRootOperator(), operandOne, result);
        rpnStack.push(operand);
    }

    @Override
    void validate(final Stack<Operand> rpnStack) throws NotEnoughOperandException {
        if (rpnStack == null || rpnStack.size() < getRequiredOperand()) {
            throw new NotEnoughOperandException();
        }
    }
}
