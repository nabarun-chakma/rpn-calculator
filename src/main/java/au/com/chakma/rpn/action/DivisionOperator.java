package au.com.chakma.rpn.action;

import au.com.chakma.rpn.constant.CommandName;
import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.BinaryOperand;
import au.com.chakma.rpn.model.Operand;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Stack;

/**
 * divides top number with the 2nd top number from the stack and push the result to the stack.
 * 
 * Created by nabarunchakma on 11/5/17.
 */
public class DivisionOperator extends Operator {

    public DivisionOperator() {
        super(CommandName.DIVISION_COMMAND, 2);
    }

    @Override
    public void execute(final Stack<Operand> rpnStack) throws NotEnoughOperandException, MissingStackException {
        validate(rpnStack);
        Operand operandOne = pop(rpnStack);
        Operand operandTwo = pop(rpnStack);
        BigDecimal result = operandOne.getResult().divide(operandTwo.getResult(), MathContext.DECIMAL64);
        BinaryOperand operand = new BinaryOperand(new DivisionOperator(), operandOne, operandTwo, result);

        rpnStack.push(operand);

    }

    @Override
    void validate(final Stack<Operand> rpnStack) throws NotEnoughOperandException {
        if (rpnStack == null || rpnStack.size() < getRequiredOperand()) {
            throw new NotEnoughOperandException();
        }
    }
}
