package au.com.chakma.rpn.action;

import au.com.chakma.rpn.constant.CommandName;
import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.BinaryOperand;
import au.com.chakma.rpn.model.NullOperand;
import au.com.chakma.rpn.model.Operand;
import au.com.chakma.rpn.model.UnaryOperand;

import java.util.Stack;

/**
 * reverses the previous operation if there is a previous operation in the stack
 *
 * Created by nabarunchakma on 12/5/17.
 */
public class UndoOperator extends Operator {

    public UndoOperator() {
        super(CommandName.UNDO_COMMAND, 0);
    }
    @Override
    public void execute(final Stack<Operand> rpnStack) throws NotEnoughOperandException, MissingStackException {
        validate(rpnStack);
        Stack<Operand> tempStack = new Stack<>();

        // clear the last result from the stack
        Operand operand = rpnStack.pop();
        while (operand instanceof NullOperand) {
            tempStack.push(operand);
            if (rpnStack.isEmpty()) {
                break;
            }
            operand = rpnStack.pop();
        }

        // TODO Keeps the operands in the Operand class as Array so that it does not have to be checked here.
        if (operand instanceof UnaryOperand) {
            rpnStack.push(((UnaryOperand) operand).getOperandOne());
        } else if (operand instanceof BinaryOperand) {
            // Should be inserted in reverse order
            rpnStack.push(((BinaryOperand) operand).getOperandTwo());
            rpnStack.push(((BinaryOperand) operand).getOperandOne());
        }

        // Put back operand from the tempStack
        while (!tempStack.isEmpty()) {
            rpnStack.push(tempStack.pop());
        }
    }

    @Override
    void validate(final Stack<Operand> rpnStack) throws NotEnoughOperandException {
        if (rpnStack == null || rpnStack.size() == 0) {
            throw new NotEnoughOperandException();
        }
    }
}
