package au.com.chakma.rpn.action;

import au.com.chakma.rpn.constant.CommandName;
import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.BinaryOperand;
import au.com.chakma.rpn.model.Operand;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * adds two top numbers from the stack and push the result to the stack.
 * 
 * Created by nabarunchakma on 10/5/17.
 */
public class AdditionOperator extends Operator {

    public AdditionOperator() {
        super(CommandName.ADDITION_COMMAND, 2);
    }

    @Override
    public void execute(final Stack<Operand> rpnStack) throws NotEnoughOperandException, MissingStackException {
        validate(rpnStack);
        Operand operandOne = pop(rpnStack);
        Operand operandTwo = pop(rpnStack);
        BigDecimal result = operandOne.getResult().add(operandTwo.getResult());
        BinaryOperand operand = new BinaryOperand(new AdditionOperator(), operandOne, operandTwo, result);

        rpnStack.push(operand);

    }

    @Override
    void validate(final Stack<Operand> rpnStack) throws NotEnoughOperandException {
        if (rpnStack == null || rpnStack.size() < getRequiredOperand()) {
            throw new NotEnoughOperandException();
        }
    }
}
