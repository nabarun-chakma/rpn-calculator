package au.com.chakma.rpn.action;

import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.Operand;

import java.util.Stack;

/**
 * an abstract definition of an operator with the basic information - name and total number of operand required
 * to execute the operator.
 *
 * Created by nabarunchakma on 10/5/17.
 */
public abstract class Operator {
    private String name;
    private int requiredOperand;

    public Operator(String name, int requiredOperand) {
        this.name = name;
        this.requiredOperand = requiredOperand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredOperand() {
        return requiredOperand;
    }

    public void setRequiredOperand(int requiredOperand) {
        this.requiredOperand = requiredOperand;
    }

    /**
     * returns the top operand from the stack and remove it from the stack.
     *
     * @param rpnStack the stack
     * @return Operand - the value from the top of the stack
     * @throws MissingStackException - if the stack is null
     * @throws NotEnoughOperandException - if the stack is empty
     */
    protected Operand pop(final Stack<Operand> rpnStack) throws MissingStackException, NotEnoughOperandException {
        if (rpnStack == null) {
            throw new MissingStackException();
        }

        if (rpnStack.isEmpty()) {
            throw new NotEnoughOperandException();
        }

        return rpnStack.pop();
    }

    /**
     * performs the operation and pushes the result to the stack. It also maintain a stack to perforn undo operation.
     * It pushes the operator first followed by its operands.
     *
     * @param rpnStack the stack of operands
     * @throws NotEnoughOperandException if there is not enough operands in the stack to perform the operation
     * @throws MissingStackException if the stack is null
     */

    public abstract void execute(final Stack<Operand> rpnStack)
        throws NotEnoughOperandException, MissingStackException;

    /**
     * validate whether current operator has enough operands to execute the operation.
     *
     * @param rpnStack the stack of operands
     * @throws NotEnoughOperandException if there is not enough operands in the stack to perform the operation
     */
    abstract void validate(final Stack<Operand> rpnStack)
        throws NotEnoughOperandException;
}
