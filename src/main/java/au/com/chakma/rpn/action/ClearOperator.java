package au.com.chakma.rpn.action;

import au.com.chakma.rpn.constant.CommandName;
import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.Operand;

import java.util.Stack;

/**
 * remove all operands from the rpn stack and undo stack.
 *
 * Created by nabarunchakma on 12/5/17.
 */
public class ClearOperator extends Operator {

    public ClearOperator() {
        super(CommandName.CLEAR_COMMAND, 0);
    }
    @Override
    public void execute(final Stack<Operand> rpnStack) throws NotEnoughOperandException, MissingStackException {
        validate(rpnStack);
        rpnStack.clear();
    }

    @Override
    void validate(final Stack<Operand> rpnStack) throws NotEnoughOperandException {
        if (rpnStack == null) {
            throw new NotEnoughOperandException();
        }
    }
}
