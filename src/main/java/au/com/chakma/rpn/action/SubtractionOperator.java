package au.com.chakma.rpn.action;

import au.com.chakma.rpn.constant.CommandName;
import au.com.chakma.rpn.exception.MissingStackException;
import au.com.chakma.rpn.exception.NotEnoughOperandException;
import au.com.chakma.rpn.model.BinaryOperand;
import au.com.chakma.rpn.model.Operand;

import java.math.BigDecimal;
import java.util.Stack;

/**
 * substacts 2nd top number from the top number and push the result to the stack.
 * 
 * Created by nabarunchakma on 11/5/17.
 */
public class SubtractionOperator extends Operator {

    public SubtractionOperator() {
        super(CommandName.SUBTRACTION_COMMAND, 2);
    }

    @Override
    public void execute(final Stack<Operand> rpnStack) throws NotEnoughOperandException, MissingStackException {
        validate(rpnStack);
        Operand operandOne = pop(rpnStack);
        Operand operandTwo = pop(rpnStack);
        BigDecimal result = operandOne.getResult().subtract(operandTwo.getResult());
        BinaryOperand operand = new BinaryOperand(new SubtractionOperator(), operandOne, operandTwo, result);

        rpnStack.push(operand);

    }

    @Override
    void validate(final Stack<Operand> rpnStack) throws NotEnoughOperandException {
        if (rpnStack == null || rpnStack.size() < getRequiredOperand()) {
            throw new NotEnoughOperandException();
        }

    }
}
