# RPN Calculator

A simple java implementation of a RPN Calculator.

## Objective

The objective of this implementation is to demonstrate
* Java Programming language
* Object Oriented Programming
* Good programming ethics
* Automated testing for Continuous Integration and Continuous Deployment

## Operations Implemented

`+` Addition

`-` Subtraction

`*` Multiplication

`/` Division

`sqrt` Square Root

`undo` Undo the previous operation

`clear` Clear the stack

## How to

### Import the code
  
  `git clone https://chakma@gitlab.com/nabarun-chakma/rpn-calculator.git`

### Build
  
  Go to your root of the project
  
  `cd rpn-calculator`
  
  Set up Gradle (No Gradle installation required)
  
  `./gradlew wrapper` - Mac or Unix
  
  `gradlew wrapper` - Windows
  
  Build the project
  
  `./gradlew clean build` - Mac or Unix
  
  `gradlew clean build` - Windows
  
### Execution
  
  Go to build directory
   
  `cd build/libs`
   
  Run the program
   
  `java -jar rpn-calculator-1.0-SNAPSHOT.jar `
  
  
## Comments
  
  Due to the time pressure the functions are not implemented or tested for edge cases.
  For example, all parameters to functions are not null checked.
  